package models

import (
	"fmt"
	"ms-bribrain-link5/helper"
)

type ResponseInfoTierNasabahV110 struct {
	PosisiTier     *string `json:"posisi_tier"`
	NamaNasabah    *string `json:"nama_nasabah"`
	Inisial        *string `json:"inisial"`
	NamaRM         *string `json:"nama_rm"`
	Cabang         *string `json:"cabang"`
	NoTelepon      *string `json:"no_telepon"`
	StatusPinjaman *string `json:"status_pinjaman,omitempty"`
	// NamaRM         *string `json:"nama_rm,omitempty"`
	// Cabang         *string `json:"cabang,omitempty"`
	// NoTelepon      *string `json:"no_telepon,omitempty"`
	// StatusPinjaman *string `json:"status_pinjaman,omitempty"`
}

// type InfoTierCurrentNasabah struct {
// 	PosisiTier  *string `json:"posisi_tier"`
// 	NamaNasabah *string `json:"nama_nasabah"`
// 	Inisial     *string `json:"inisial_nasabah"`
// }

func MappingInfoTierCurrentNasabahV110(data *GetDetailQueryStruct) *ResponseInfoTierNasabahV110 {
	intTier := helper.Int64NullableToInt(data.PosisiTier)
	posisiTier := fmt.Sprintf("Tier %d", intTier)

	// nama := helper.StringNullableToString(data.NamaNasabahRekan)
	inisial := mappingInisial(data.NamaNasabahRekan)

	res := &ResponseInfoTierNasabahV110{

		PosisiTier:     helper.StringToStringNullable(posisiTier),
		NamaNasabah:    data.NamaNasabahRekan,
		Inisial:        inisial,
		NamaRM:         data.SnameRmPinjaman,
		Cabang:         data.UnitRmPinjaman,
		NoTelepon:      data.PhonenumberRmPinjaman,
		StatusPinjaman: mappingFlagPinjaman(data.FlagPinjaman),
	}
	return res
}

type ResponseInfoTierNasabahV110Dto struct {
	HierarkiTier []*ResponseInfoTierNasabahV110 `json:"hierarki_tier"`
}

func MappingResponseInfoTierNasabahV110Dto(dataTier []*ResponseInfoTierNasabahV110, dataNasabah *ResponseInfoTierNasabahV110) *ResponseInfoTierNasabahV110Dto {
	res := &ResponseInfoTierNasabahV110Dto{
		HierarkiTier: append(dataTier, dataNasabah),
	}
	return res
}

func mappingFlagPinjaman(data *int64) *string {
	var res *string

	if helper.Int64NullableToInt(data) == 1 {
		res = nil
	} else if helper.Int64NullableToInt(data) == 0 || data == nil {
		res = helper.StringToStringNullable("Belum memiliki pinjaman")
	}

	return res
}

func mappingInisial(data *string) *string {
	var res *string

	if data == nil || data == helper.StringToStringNullable("") {
		res = nil
	} else {
		nama := helper.StringNullableToString(data)
		res = helper.StringToStringNullable(getInitialDetail(nama))
	}

	return res
}

func MappingResponseInfoTierNasabahInRowV110(data *GetDetailQueryStruct) []*ResponseInfoTierNasabahV110 {
	// namarm1 := helper.StringNullableToString(data.SnameRmPinjamanTier1)
	// namarm2 := helper.StringNullableToString(data.SnameRmPinjamanTier2)
	// namarm3 := helper.StringNullableToString(data.SnameRmPinjamanTier3)
	// namarm4 := helper.StringNullableToString(data.SnameRmPinjamanTier4)

	namanasabaht1 := data.NamaNasabahTier1
	namanasabaht2 := data.NamaNasabahTier2
	namanasabaht3 := data.NamaNasabahTier3
	namanasabaht4 := data.NamaNasabahTier4

	// var keterangan, namaUnit, namaRM, nomorRM *string

	var inisialt1, inisialt2, inisialt3, inisialt4 *string
	var unitrm1, unitrm2, unitrm3, unitrm4 *string
	var namarmt1, namarmt2, namarmt3, namarmt4 *string
	var phonenumberrmt1, phonenumberrmt2, phonenumberrmt3, phonenumberrmt4 *string
	var statuspinjaman1, statuspinjaman2, statuspinjaman3, statuspinjaman4 *string

	tierCurrentNasabah := helper.Int64NullableToInt(data.PosisiTier)
	switch tierCurrentNasabah {
	case 5:

		inisialt4 = mappingInisial(namanasabaht4)
		unitrm4 = data.UnitRmTier4
		statuspinjaman4 = mappingFlagPinjaman(data.FlagPinjamanTier4)
		namarmt4 = data.SnameRmPinjamanTier4
		phonenumberrmt4 = data.PhonenumberTier4

		inisialt3 = mappingInisial(namanasabaht3)
		unitrm3 = data.UnitRmTier3
		statuspinjaman3 = mappingFlagPinjaman(data.FlagPinjamanTier3)
		namarmt3 = data.SnameRmPinjamanTier3
		phonenumberrmt3 = data.PhonenumberTier3

		inisialt2 = mappingInisial(namanasabaht2)
		unitrm2 = data.UnitRmTier2
		statuspinjaman2 = mappingFlagPinjaman(data.FlagPinjamanTier2)
		namarmt2 = data.SnameRmPinjamanTier2
		phonenumberrmt2 = data.PhonenumberTier2

		inisialt1 = mappingInisial(namanasabaht1)
		unitrm1 = data.UnitRmTier1
		statuspinjaman1 = mappingFlagPinjaman(data.FlagPinjamanTier1)
		namarmt1 = data.SnameRmPinjamanTier1
		phonenumberrmt1 = data.PhonenumberRmTier1

	case 4:

		inisialt3 = mappingInisial(namanasabaht3)
		unitrm3 = data.UnitRmTier3
		statuspinjaman3 = mappingFlagPinjaman(data.FlagPinjamanTier3)
		namarmt3 = data.SnameRmPinjamanTier3
		phonenumberrmt3 = data.PhonenumberTier3

		inisialt2 = mappingInisial(namanasabaht2)
		unitrm2 = data.UnitRmTier2
		statuspinjaman2 = mappingFlagPinjaman(data.FlagPinjamanTier2)
		namarmt2 = data.SnameRmPinjamanTier2
		phonenumberrmt2 = data.PhonenumberTier2

		inisialt1 = mappingInisial(namanasabaht1)
		unitrm1 = data.UnitRmTier1
		statuspinjaman1 = mappingFlagPinjaman(data.FlagPinjamanTier1)
		namarmt1 = data.SnameRmPinjamanTier1
		phonenumberrmt1 = data.PhonenumberRmTier1

	case 3:

		inisialt2 = mappingInisial(namanasabaht2)
		unitrm2 = data.UnitRmTier2
		statuspinjaman2 = mappingFlagPinjaman(data.FlagPinjamanTier2)
		namarmt2 = data.SnameRmPinjamanTier2
		phonenumberrmt2 = data.PhonenumberTier2

		inisialt1 = mappingInisial(namanasabaht1)
		unitrm1 = data.UnitRmTier1
		statuspinjaman1 = mappingFlagPinjaman(data.FlagPinjamanTier1)
		namarmt1 = data.SnameRmPinjamanTier1
		phonenumberrmt1 = data.PhonenumberRmTier1

	case 2:

		inisialt1 = mappingInisial(namanasabaht1)
		unitrm1 = data.UnitRmTier1
		statuspinjaman1 = mappingFlagPinjaman(data.FlagPinjamanTier1)
		namarmt1 = data.SnameRmPinjamanTier1
		phonenumberrmt1 = data.PhonenumberRmTier1

	case 1:
		inisialt4 = helper.StringToStringNullableNullValue("")
		inisialt3 = helper.StringToStringNullableNullValue("")
		inisialt2 = helper.StringToStringNullableNullValue("")
		inisialt1 = helper.StringToStringNullableNullValue("")
	}
	// inisial := getInitialDetail(nama)
	t1 := &ResponseInfoTierNasabahV110{
		PosisiTier:     helper.StringToStringNullable("Tier 1"),
		NamaNasabah:    data.NamaNasabahTier1,
		Inisial:        inisialt1,
		NamaRM:         namarmt1,
		Cabang:         unitrm1,
		NoTelepon:      phonenumberrmt1,
		StatusPinjaman: statuspinjaman1,
	}

	t2 := &ResponseInfoTierNasabahV110{
		PosisiTier:     helper.StringToStringNullable("Tier 2"),
		NamaNasabah:    data.NamaNasabahTier2,
		Inisial:        inisialt2,
		NamaRM:         namarmt2,
		Cabang:         unitrm2,
		NoTelepon:      phonenumberrmt2,
		StatusPinjaman: statuspinjaman2,
	}

	t3 := &ResponseInfoTierNasabahV110{
		PosisiTier:     helper.StringToStringNullable("Tier 3"),
		NamaNasabah:    data.NamaNasabahTier3,
		Inisial:        inisialt3,
		NamaRM:         namarmt3,
		Cabang:         unitrm3,
		NoTelepon:      phonenumberrmt3,
		StatusPinjaman: statuspinjaman3,
	}

	t4 := &ResponseInfoTierNasabahV110{
		PosisiTier:     helper.StringToStringNullable("Tier 4"),
		NamaNasabah:    data.NamaNasabahTier4,
		Inisial:        inisialt4,
		NamaRM:         namarmt4,
		Cabang:         unitrm4,
		NoTelepon:      phonenumberrmt4,
		StatusPinjaman: statuspinjaman4,
	}

	dataTier := make([]*ResponseInfoTierNasabahV110, 0)
	switch tierCurrentNasabah {
	case 5:
		dataTier = append(dataTier, t1, t2, t3, t4)
	case 4:
		dataTier = append(dataTier, t1, t2, t3)
	case 3:
		dataTier = append(dataTier, t1, t2)
	case 2:
		dataTier = append(dataTier, t1)
	case 1:
		dataTier = append(dataTier)
	}
	// dataTier = append(dataTier, t1, t2, t3, t4)
	return dataTier
}
